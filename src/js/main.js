import '../scss/main.scss';
import '../index.html';
import { Swiper, Controller, Pagination, Navigation } from 'swiper';

Swiper.use([Controller, Pagination, Navigation]);

const slides = document.getElementsByClassName('tab-slides');
const tabSlide = [];

function createArrayNameSlides(el) {
  for (let i = 0; i < el.length; i++) {
    tabSlide.push(el[i].getAttribute('data-title'));
  }
}
createArrayNameSlides(slides);

document.querySelectorAll('.project').forEach(n => {

  const swiper = new Swiper(n.querySelector('.main-slider'), {
    spaceBetween: 20,
    loop: true,

    pagination: {
      el: '.swiper-pagination, .swiper-pagination-img',
      clickable: true,
      renderBullet: function (index, className) {
        return '<button class="' + className + '">' + (tabSlide[index]) + '</button>';
      },
    },
    navigation: {
      nextEl: '.slider__arrow-r',
      prevEl: '.slider__arrow-l',
    },
  });
});

document.querySelectorAll('.swiper-slide__inner').forEach(n => {

  const sliderInner = new Swiper(n.querySelector('.slider-img'), {
    slidesPerView: 1.3,
    spaceBetween: 20,
    loop: true,
    nested: true,

    navigation: {
      nextEl: '.image-arrow-r',
      prevEl: '.image-arrow-l',
    },
  });
  console.log(sliderInner);
});